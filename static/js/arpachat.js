var wsocket = null;
var wsocketUrlPath = '/chat-server';

const webSocketOnMessage = (event) => {
    var datum = event.data;
    var msg = JSON.parse(datum);
    var chatBox = $('#chat-div');
    var msgDiv = $('<div></div>');
    var bNode = $('<b></b>');
    var nameNode = document.createTextNode(msg.name + ': ');
    var msgNode = document.createTextNode(msg.text);
    console.log('message', datum);
    bNode.append(nameNode);
    msgDiv.append(bNode).append(msgNode);
    return chatBox.append(msgDiv);
};

const webSocketConnect = () => {
    var url = 'ws' + (window.location.protocol === 'https:' ? 's' : '') + '://' + window.location.host + wsocketUrlPath;
    wsocket = new WebSocket(url);
    wsocket.onmessage = webSocketOnMessage;
    return console.log('websocket connect to server at ' + url);
};

const connectClick = () => {
    return webSocketConnect();
};

const enterClick = () => {
    if (wsocket) {
        var nameBox = $('#username');
        if (nameBox.length) {
            wsocket.send(JSON.stringify({ type : 'enter', value : $(nameBox).val() }));
        };
    };
    return true;
};

const messageClick = () => {
    if (wsocket) {
        var msgBox = $('#msg-input');
        if (msgBox.length) {
            wsocket.send(JSON.stringify({ type : 'message', value : $(msgBox).val() }));
        };
    };
    return true;
};
