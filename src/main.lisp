(in-package :cl-user)
(defpackage arpachat
  (:use :cl)
  (:import-from :arpachat.config
                :config)
  (:import-from :clack
                :clackup)
  (:export :start
           :stop
           :main))
(in-package :arpachat)

(defvar *appfile-path*
  (asdf:system-relative-pathname :arpachat #P"app.lisp"))

(defvar *handler* nil)

;; changed to make port 3000 as default
(defun start (&rest args &key server (port 3000 portp) debug &allow-other-keys)  
  (declare (ignore server debug))
  (when *handler*
    (restart-case (error "Server is already running.")
      (restart-server ()
        :report "Restart the server"
        (stop))))
  (setf *handler*
        (apply #'clackup *appfile-path*
               (if portp
                   args
                   (list* :port port args)))))

(defun stop ()
  (prog1
      (clack:stop *handler*)
    (setf *handler* nil)))

(defun main ()
  (start :port (parse-integer (uiop:getenv "PORT")))
  ;; with bordeaux-threads
  (handler-case (bt:join-thread (find-if (lambda (th)
                                           (search "hunchentoot" (bt:thread-name th)))
                                         (bt:all-threads)))
    (#+sbcl sb-sys:interactive-interrupt
      #+ccl  ccl:interrupt-signal-condition
      #+clisp system::simple-interrupt-condition
      #+ecl ext:interactive-interrupt
      #+allegro excl:interrupt-signal
      () (progn
           (format *error-output* "Aborting.~&")
           (clack:stop *handler*)
           (uiop:quit 1)))))
