(defsystem "arpachat"
  :version "0.2.0"
  :author "Momozor"
  :license "MIT"
  :depends-on ("hunchentoot"
               "clack"
               "lack"
               "caveman2"
               "envy"
               "cl-ppcre"
               "uiop"
               "websocket-driver"
               "log4cl" 
               "jonathan"
               "string-case"
               "alexandria"

               ;; for @route annotation
               "cl-syntax-annot"

               ;; HTML Template
               "djula"

               ;; for DB
               "datafly"
               "sxql"

               ;; for testing
               "rove")
  :components ((:module "src"
                        :components
                        ((:file "main" :depends-on ("config" "view" "db"))
                         (:file "web" :depends-on ("view" "chat"))
                         (:file "view" :depends-on ("config"))
                         (:file "db" :depends-on ("config"))
                         (:file "chat")
                         (:file "config"))))
  :description "Web chat software"
  :in-order-to ((test-op (test-op "arpachat/tests"))))


(defsystem "arpachat/tests"
  :author "Momozor"
  :license "MIT"
  :depends-on ("arpachat"
               "rove")
  :components ((:module "tests"
                        :components
                        ((:file "chat"))))
  :description "Test system for arpachat"  
  :perform (test-op (op c) (symbol-call :rove :run c)))
