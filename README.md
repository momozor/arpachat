[![Build Status](https://travis-ci.org/momozor/arpachat.svg?branch=master)](https://travis-ci.org/momozor/arpachat)
# Arpachat
A web chat application using Websockets and Common Lisp, forked from [websocket-caveman-chat](https://github.com/aarvid/websocket-caveman-chat).

## Installation
* Make sure you have *Quicklisp* installed
* `git clone https://github.com/momozor/arpachat /path/to/quicklisp/local-projects/`
* Launch your Lisp interpreter and run `(ql:quickload :arpachat)`

## Usage
* `(ql:quickload :arpachat)`
* `(arpachat:start)` to start the server at port 3000
* Launch a web browser and navigate to http://localhost:3000 to use the chat application.
* Enjoy! :)

## Authors
* Momozor

## License
This project is licensed under the MIT license. See LICENSE file for more details
